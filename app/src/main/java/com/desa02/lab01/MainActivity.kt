package com.desa02.lab01

import android.content.res.ColorStateList
import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        btnProcesar.setOnClickListener {
            val edad = if ( edtEdad.text.toString().equals("") ) 0 else Integer.valueOf( edtEdad.text.toString() )
            if ( edad > 0 )
                //tvResultado.setText( iff(edad < 18 , "Usted es menor de edad.","Usted es mayor de edad.") )
                {
                    if(edad < 18){
                        tvResultado.setText(  "Usted es menor de edad.")
                        tvResultado.setBackgroundColor(Color.parseColor("#FFFFEB3B"))
                    }
                    else{
                        tvResultado.setText(  "Usted es mayor de edad.")
                        tvResultado.setBackgroundColor(Color.parseColor("#FF2196F3"))
                    }
                }
            else
                Toast.makeText( this,"Digitar la edad correcta.",Toast.LENGTH_LONG ).show()
        }

        edtEdad.addTextChangedListener( object: TextWatcher{
            override fun afterTextChanged(p0: Editable?) {
                tvResultado.setText( "[Resultado]")
                tvResultado.setBackgroundColor(Color.parseColor("#FFFFFF"))
            }
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }
            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }
        } )
    }

    fun<T> iff(condition: Boolean, result1: T, result2: T) = if (condition) result1 else result2

}